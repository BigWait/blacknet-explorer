Blacknet = angular.module('blacknet', ['ngResource']);
Blacknet.config(function($interpolateProvider, $filterProvider) {
    $interpolateProvider.startSymbol('${');
    $interpolateProvider.endSymbol('}');
    let isMobile = false;

    if(document.body.clientWidth < 500){
        isMobile = true;
    }
    $filterProvider.register('currency', function(){
        return function(text){

            let str = new BigNumber(text).dividedBy(1e8).toFixed(8);
            
            if( (+text) < 1e8 ){
                return str;
            }

            return numeral(str).format('0,0.00000000');
        }
    });

    $filterProvider.register('touch', function(){
        return function(text){
            if(isMobile) return new BigNumber(numeral(text).value()).toFixed(2);
            return text;
        }
    });


    $filterProvider.register('toFixed', function(){
        return function(text){
            return new BigNumber(text).toFixed(8);
        }
    });

    $filterProvider.register('percent', function(){
        return function(text){
            return new BigNumber(text).dividedBy(Supply).dividedBy(1e8).times(100).toFixed(2);
        }
    });
    $filterProvider.register('txType', function(){
        return function(text){
            let typeStr = getTxType(text);

            return typeStr;
        }
    });
    $filterProvider.register('tansferType', function(){
        return function(tx){
            let typeStr = getTxType(text);
            if(tx.typeStr == 'Transfer' && address){
                tx.typeStr = address == tx.from ? "Sent to" : 'Received from';
            }
            return typeStr;
        }
    });
    $filterProvider.register('timestr', function(){
        return function(text){
            let time = +text;
            if(text && String(text).length == 10){
                time *= 1000;
            }
            if(!text) return text;
            return unix_to_local_time(time, false);
        }
    });
    $filterProvider.register('realstr', function(){
        return function(text){
            let time = +text;
            if(text && text.length == 10){
                time *= 1000;
            }
            if(!text) return text;
            return unix_to_local_time(time, true, isMobile);
        }
    });
    $filterProvider.register('format_number', function(){
        return function(text){
            return numeral(text).format('0,0');
        }
    });
});
Blacknet.factory('blocks', ['$resource', function($resource) {
    
    return $resource('/api/recent_blocks');
}]);

// add Blacknet
Blacknet.directive('loadScript', function() { 
	return { 
		restrict: 'EA', 
		terminal: true, 
		link: function(scope, element, attr) { 
			// if (attr.ngSrc) { 
			// 	var s = document.createElement('script'); 
			// 	s.src = attr.ngSrc; 
			// 	document.body.appendChild(s); 
			// }
		}
	}; 
})
function unix_to_local_time(unix_timestamp, real, isMobile) {

    let date = new Date(unix_timestamp), now = Date.now();
    let subSeconds = (now - (+date))/1000;
    if(subSeconds <  60 && real){
        return parseInt(subSeconds) + ' seconds ago';
    }else if(subSeconds < 60 * 60 && real){
        return Math.ceil(subSeconds / 60) + ' minutes ago';
    }

    let hours = "0" + date.getHours();
    let minutes = "0" + date.getMinutes();
    let seconds = "0" + date.getSeconds();
    let day = date.getDate();
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let prefix = '';
    
    if(!isMobile) prefix = year + "-" ;

    return prefix + ('0' + month).substr(-2) + "-" +
        ('0' + day).substr(-2) + " " + hours.substr(-2) + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
}

function txs(transactions, address) {

    return transactions.map(tx => {
        tx.data = typeof tx.data == "string" ? JSON.parse(tx.data) : tx.data;
        tx.typeStr = getTxType(tx.type);
        if (!tx.data) {
            tx.data = { amount: tx.amount };
        }
        if(tx.typeStr == 'Transfer' && address){
            tx.typeStr = address == tx.from ? "Sent to" : 'Received from';
        }
        tx.data.amount = new BigNumber(tx.data.amount).dividedBy(1e8).toFixed(8);
        return tx;
    });
}

var txType = [
    "Transfer",
    "Burn",
    "Lease",
    "CancelLease",
    "Bundle",
    "CreateHTLC",
    "UnlockHTLC",
    "RefundHTLC",
    "SpendHTLC",
    "CreateMultisig",
    "SpendMultisig",
    "WithdrawFromLease",
    "ClaimHTLC"];
    txType[16] = "MultiData";
    txType[254] = "PoS Generated";
    txType["all"] = "All";

function getTxType(type){

    if(type === undefined) return '';

    return txType[type] || 'Genesis';
}
