

const defaultSupply = 10e8;
const API = require('./api');
let startHeight = 0;
let blankSign = new Array(128).join('0');

async function process() {

    let blockHash, block, dbbk, supply = defaultSupply;
    let lastBlock = await Block.findOne({}).sort({ height: 'desc' });;

    if (lastBlock) {
        startHeight = lastBlock.height;
        supply = getSupplyWithHeight(startHeight);
    }

    console.log('Syncing start', startHeight)
    console.time('Sync')
    while (++startHeight) {

        blockHash = await API.getBlockHash(startHeight);
        if (blockHash == 'block not found') {
            console.timeEnd('Sync')
            break;
        }

        block = await API.getBlock(blockHash, true);

        if (block == 'invalid hash') {
            console.timeEnd('Sync')
            break;
        }

        dbbk = await Block.findOne({ blockHash });

        if (!dbbk && block.transactions) {
            dbbk = new Block(getBlock(block, startHeight, blockHash));
            dbbk.blockHash = blockHash;
            dbbk.timestr = unix_to_local_time(dbbk.time);

            if (block && block.transactions.length > 0) {
                await txSave(dbbk);
            }
        }
        // bug fix
        if(!dbbk){
            break;
        }

        let reward = getReward(supply);

        if(startHeight > 485107) reward = getNewReward(supply);

        supply += reward;

        global.Supply = supply

        let blockReward = reward.toFixed(9).slice(0, -1);

        let fees = getFees(dbbk.transactions),
            fee = fees + reward * 1e8;

        let tx = {
            size: 142,
            signature: blankSign,
            from: block.generator,
            seq: 0,
            blockHash: blockHash,
            fee: fees,
            type: 254,
            time: unix_to_local_time(block.time),
            data: {
                blockHeight: startHeight,
                blockHash,
                amount: fee
            }
        }
        tx = new Transaction(tx);
        tx.save();
        updateBalance(tx.from, 1);
        dbbk.reward = blockReward;
        dbbk.save();
        if (startHeight % 1000 == 0) {
            console.log(startHeight)
            console.timeEnd('Sync')
            console.time('Sync')
        }
    }

    setTimeout(process, 2000);
}

try {
    setTimeout(process, 2000);
} catch (error) {
    console.log(error.stack);
    setTimeout(process, 2000);
}


async function updateBalance(address, isGenerator) {

    let account = await Account.findOne({ address }), isNew = false;

    let data = await API.getBalance(address);
    
    if (!account) {

        let instance = {
            address,
            txamount: 1,
            blocks: 0
        };
        account = new Account(instance);
        isNew = true;
    }

    if (isNaN(data.balance) == false) {
        account.balance = parseInt(data.balance);
        account.confirmedBalance = parseInt(data.confirmedBalance);
        account.stakingBalance = parseInt(data.stakingBalance);
        account.seq = data.seq;
        if (isGenerator) account.blocks += 1;
        await account.save();
        console.log(` update leases info: ${account.address}`);
        await updateLeaseBalance(account);
    }
}


async function updateLeaseBalance(instance) {

    let outleases, inLeases, canceloutleases, cancelinLeases;
    let outLeasesBalance = 0, inLeasesBalance = 0, address = instance.address;

    outleases = await Transaction.find({ type: 2, from: address });
    inLeases = await Transaction.find({ type: 2, to: address });

    canceloutleases = await Transaction.find({ type: 3, from: address });
    cancelinLeases = await Transaction.find({ type: 3, to: address });

    if (outleases.length == 0 && inLeases.length == 0) return;
    for (let tx of outleases) {
        outLeasesBalance += tx.data.amount;
    }

    for (let tx of canceloutleases) {
        outLeasesBalance -= tx.data.amount;
    }

    for (let tx of inLeases) {
        inLeasesBalance += tx.data.amount;
    }

    for (let tx of cancelinLeases) {
        inLeasesBalance -= tx.data.amount;
    }

    instance.outLeasesBalance = outLeasesBalance < 0 ? 0 : outLeasesBalance;
    instance.inLeasesBalance = inLeasesBalance < 0 ? 0 : inLeasesBalance;
    instance.realBalance = instance.outLeasesBalance + instance.confirmedBalance; 
    await instance.save();
}



async function txSave(block) {

    let accountMap = {};

    for (let tx of block.transactions) {

        let dbtx;

        tx.data = JSON.parse(tx.data);
        dbtx = await Transaction.findOne({ txid: tx.hash });

        if (dbtx == null) {

            tx = getTransaction(tx, block.blockHash, block.height);
            tx = new Transaction(tx);
            tx.time = block.time;
            await tx.save();

            accountMap[tx.from] = true;
            accountMap[tx.to] = true;

            // await updateLeaseBalance(tx);

        } else {
            console.log(dbtx.txid + ' is exist');
        }
    }

    for (let address in accountMap) {
        await updateBalance(address);
    }
}

function getBlock(data, height, blockHash) {
    let block = {};

    block.size = data.size;
    block.blockHash = blockHash;
    block.height = height;
    block.version = data.version;
    block.previous = data.previous;
    block.time = unix_to_local_time(data.time);
    block.generator = data.generator;
    block.contentHash = data.contentHash;
    block.signature = data.signature;
    block.transactions = data.transactions.map((tx) => JSON.parse(tx));
    return block;
}

function getTransaction(json, blockHash, blockHeight) {
    let tx = {};

    tx.txid = json.hash;
    tx.size = json.size;
    tx.signature = json.signature;
    tx.from = json.from;
    tx.seq = json.seq;
    tx.blockHash = blockHash;
    tx.fee = json.fee;
    tx.type = json.type;
    tx.data = json.data;
    tx.to = json.data.to;
    tx.amount = json.data.amount;
    tx.data.blockHeight = blockHeight

    return tx;
}



function unix_to_local_time(unix_timestamp) {

    return unix_timestamp;
}
function getSupplyWithHeight(height) {

    let supply = 10e8, start = 0;

    if (height == start) return supply;

    while (start++ < height) {

        let reward = getReward(supply);
        if(start > 485107) reward = getNewReward(supply);;
        supply += reward;
    }

    return supply;
}

function getReward(supply) {
    let magicNumber = 492750 * 100;
    return supply / magicNumber;
}

function getNewReward(supply) {
    return supply / (100 *365 * 24 * 60 * 60 / 16);
}
function getFees(txns) {
    let fees = 0;
    for (let tx of txns) {
        fees += tx.fee;
    }
    return fees;
}

