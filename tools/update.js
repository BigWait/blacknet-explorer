


require('../lib/mongo');
const API = require('../lib/api');

async function updateWithModel(model) {
    
    let index = 0;
    while(true){

        let accounts = await model.find({}).skip(index * 100).limit(100);

        if(accounts.length == 0) break;

        for(let account of accounts){
            
            await updateBalance(account);
        }
        index++;
    }

}


async function updateBalance(instance) {

    let address = instance.address;

    let data = await API.getBalance(address);
   
    if(isNaN(data.balance) == false){
        instance.balance = parseInt(data.balance);
        instance.confirmedBalance = parseInt(data.confirmedBalance);
        instance.stakingBalance = parseInt(data.stakingBalance);
        instance.seq = data.seq;
        await instance.save();
        // await updateLeaseBalance(instance);
    }
}


async function update(){

    await updateWithModel(Account);
    process.exit(0);
}




async function updateLeaseBalance(instance) {

    let outleases, inLeases, canceloutleases, cancelinLeases;
    let outLeasesBalance = 0, inLeasesBalance = 0, address = instance.address;

    outleases = await Transaction.find({ type: 2, from: address });
    inLeases = await Transaction.find({ type: 2, to: address });

    canceloutleases = await Transaction.find({ type: 3, from: address });
    cancelinLeases = await Transaction.find({ type: 3, to: address });

    if (outleases.length == 0 && inLeases.length == 0) return;
    for (let tx of outleases) {
        outLeasesBalance += tx.data.amount;
    }

    for (let tx of canceloutleases) {
        outLeasesBalance -= tx.data.amount;
    }

    for (let tx of inLeases) {
        inLeasesBalance += tx.data.amount;
    }

    for (let tx of cancelinLeases) {
        inLeasesBalance -= tx.data.amount;
    }

    instance.outLeasesBalance = outLeasesBalance < 0 ? 0 : outLeasesBalance;
    instance.inLeasesBalance = inLeasesBalance < 0 ? 0 : inLeasesBalance;
    instance.realBalance =  instance.confirmedBalance + instance.outLeasesBalance;

    await instance.save();
}


update();