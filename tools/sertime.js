


require('../lib/mongo');


async function sertimeWithModel(model) {
    
    let index = 0;
    while(true){

        let blocks = await model.find({}).skip(index * 100).limit(100);

        if(blocks.length == 0) break;
        console.log('index is ' + index);
        for(let block of blocks){
            await sertime(block);
        }
        index++;
    }

}


async function sertimeData(){

    await sertimeWithModel(Transaction);
    await sertimeWithModel(Block);
    process.exit(0);
}

async function sertime(instance){

    instance.time = +new Date(instance.time);
    if(!isNaN(instance.time) && instance.time != NaN){
        await instance.save();
    }
}

sertimeData();