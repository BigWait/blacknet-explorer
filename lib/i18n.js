

let i18nData = {
    en: require('./i18n/en.json'),
    zh_CN: require('./i18n/zh_CN.json'),
    ja: require('./i18n/ja.json'),
    sk: require('./i18n/sk.json')
};

module.exports = async function (ctx, next) {





    if (ctx.headers.accept.indexOf('text/html') !== -1) {

        let lang = ctx.headers['accept-language'].split(','), localeData = i18nData.en;

        let locale = 'en';

        for (let l of lang) {

            if (lang.indexOf('zh') !== -1) {
                locale = 'zh_CN';
                break;
            }
            if (lang.indexOf('ja') !== -1) {
                locale = 'ja';
                break;
            }
            if (lang.indexOf('sk') !== -1) {
                locale = 'sk';
                break;
            }
        }

        let data = i18nData[locale];

        localeData = {
            ...localeData,
            ...data,
            locale
        };
        ctx.i18n = localeData;
    }

    await next();


};
