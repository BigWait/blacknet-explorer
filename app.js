const path = require('path');
const Koa = require('koa');
const serve = require('koa-static');
const views = require('koa-views');
const app = new Koa();
const router = require('./route');
const apirouter = require('./route/api');
const nunjucks = require('nunjucks')
const env = new nunjucks.Environment();

require('./lib/mongo');

const initapp = require('./lib/app');
const i18n = require('./lib/i18n');

async function start() {
    
    await initapp();

    app.use(async (ctx, next) => {
        const start = Date.now();

        await next();
        if (ctx.status == 404) {
            await ctx.render('404', {i18n: ctx.i18n });
        }
        if (ctx.status >= 500) {
            await ctx.render('500', {i18n: ctx.i18n });
        }
        const ms = Date.now() - start;
        console.log(`${ctx.method} ${ctx.url} - ${ms} ms`);
        ctx.append('app_version', global.process.env.APP_VERSION ? global.process.env.APP_VERSION : 'origin');
    });

    env.addGlobal('overview', global.overview);

    app.use(i18n).use(views(path.join(__dirname, 'views'), {
        options: {
            nunjucksEnv: env
        },
        extension: 'njk',
        map: {
            njk: 'nunjucks'
        }
    }));
    app.use(serve(__dirname + '/static'));

    app.use(router.routes()).use(apirouter.routes()).use(router.allowedMethods());

    app.listen(3000);
}

start();