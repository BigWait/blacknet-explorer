


const Router = require('koa-router');
const router = new Router();
const API = require('../lib/api');
const Utils = require('../lib/utils');

router.get('/', async (ctx, next) => {

    await ctx.render('recent_blocks', { recent: true, i18n: ctx.i18n });
});

router.get('/alias', async (ctx, next) => {
    await ctx.render('alias', { i18n: ctx.i18n });
});
router.get('/alias/:address', async (ctx, next) => {
    await ctx.render('alias', { address: ctx.params.address, i18n: ctx.i18n });
});

router.get('/forkV2', async (ctx, next) => {
    await ctx.render('forkV2', { i18n: ctx.i18n });
});

router.get('/:keyword', async (ctx, next) => {

    let keyword = ctx.params.keyword;

    if (/\d+/.test(keyword)) {
        await renderBlock(ctx, keyword);
    }

    if (['about', 'recent_blocks', 'recent_transactions', 'top_accounts'].indexOf(keyword) !== -1) {

        let ret = { i18n: ctx.i18n };
        ret[keyword] = true;

        await ctx.render(keyword, ret);
    }

    if (keyword.length == 67 && /^blacknet[a-zA-Z0-9]{59}$/.test(keyword)) {

        await renderAccount(ctx, keyword);
    }

    if (keyword.length == 64) {

        let instance = await Block.findOne({ blockHash: keyword.toUpperCase() });
        if (instance) {
            await ctx.render('block', { block: instance, i18n: ctx.i18n });
        } else {
            await renderTx(ctx, keyword);
        }
    }
    await next();
});

router.get('/api', async (ctx, next) => {


    await ctx.render('apis', { apis: true, i18n: ctx.i18n });
});

router.get('/privacy.html', async (ctx, next) => {
    await ctx.render('privacy', { i18n: ctx.i18n });
});


router.get('/recent_blocks', async (ctx, next) => {


    await ctx.render('recent_blocks', { recent: true, i18n: ctx.i18n });
});

router.get('/recent_transactions', async (ctx, next) => {

    await ctx.render('recent_transactions', { recent_transactions: true, i18n: ctx.i18n });
});


router.get('/tx/:hash', async (ctx, next) => {

    await renderTx(ctx, ctx.params.hash);
});

router.get('/search', async (ctx, next) => {
    let key = ctx.query.q;
    if (Utils.verifyAccount(key)) {
        return ctx.redirect('/' + key);
    }
    if (Utils.verifyBlockNumber(key)) {
        return ctx.redirect('/' + key);
    }
    if (Utils.verifyHash(key)) {
        let block = await Block.findOne({ blockHash: key.toUpperCase() });
        if (block) {
            return ctx.redirect('/block/' + key);
        }
        let tx = await Transaction.findOne({ txid: key.toUpperCase() });
        if (tx) {
            return ctx.redirect('/tx/' + key);
        }
    }
    next()
});


router.get('/top_accounts', async (ctx, next) => {
    let order = ctx.query.order_by || 'rich';

    await ctx.render('top_accounts', { top_accounts: true, order, i18n: ctx.i18n });
});

router.get('/block/:height', async (ctx, next) => {

    let height = ctx.params.height;
    await renderBlock(ctx, height);
});

router.get('/network', network);
router.get('/dashboard', network);
async function network(ctx, next){

    let peers = await Peer.find({});
    let currentPeers = await API.getPeerInfo();

    if(currentPeers.error) {
        return await ctx.render('peers', { currentPeers: [], peers, dashboard: true, i18n: ctx.i18n });
    }
    for (var cpeer of currentPeers) {

        let index = cpeer.remoteAddress.lastIndexOf(':');
        let ip = cpeer.remoteAddress.slice(0, index);
        let dpeer = await Peer.findOne({ ip });

        cpeer.ip = ip;
        cpeer.port = cpeer.remoteAddress.slice(index + 1);
        if (dpeer) {
            cpeer.location = dpeer.location;
        } else {
            cpeer.location = {};
        }

    }
    await ctx.render('peers', { currentPeers, peers, network: true, i18n: ctx.i18n });
}

router.get('/account/:address', async (ctx, next) => {
    
    await renderAccount(ctx, ctx.params.address);
});


async function renderBlock(ctx, hash){

    let block;
    if(Utils.verifyBlockNumber(hash)){
        block = await Block.findOne({ height: hash });
    }else if(Utils.verifyHash(hash)){
        block = await Block.findOne({ blockHash: String(hash).toUpperCase() });
    }

    if(!block){
        return await ctx.render('404', {msg: "Block not found", i18n: ctx.i18n})
    }
    block.transactions = await Transaction.find({blockHash: block.blockHash});
    
    await ctx.render('block', { block, i18n: ctx.i18n });
}

async function renderAccount(ctx, address){
    
    let type = ctx.query.type || 'all';
    let account = await Account.findOne({ address });

    if (!account) {
        await ctx.render('404', { msg: `account:${address} not found` , i18n: ctx.i18n});
    } else {
        account.txamount = await Transaction.countDocuments({$or: [{ from: address }, { to: address }]});
        await ctx.render('account', { account, address, type, i18n: ctx.i18n });
    }
}


async function renderTx(ctx, hash){

    let query = { txid: hash.toUpperCase() };
    let tx = await Transaction.findOne(query).lean();
    if (!tx) {
        let txpool = await getTxPool();
        if (txpool.length > 0) {
            for (let obj of txpool) {
                if (obj.hash == hash.toUpperCase()) {
                    tx = obj;
                    tx.txid = tx.hash;
                    tx.to = tx.data.to;
                    tx.txpool = true;
                    tx.amount = tx.data.amount;
                    tx.message = tx.data.message;
                    tx.data.blockHeight = 0;
                    tx.confirmations = 0;
                }
            }
        }
    }
    if (!tx) {
        return next()
    }else{
        let block = await Block.findOne({}).sort({ height: -1 });
        tx.confirmations = block.height - tx.data.blockHeight + 1;
    }
    await ctx.render('tx', {i18n: ctx.i18n, tx});
}
module.exports = router;

async function getTxPool() {

    let txpool = await API.getTxPool();
    let txns = [];

    if (txpool.tx || txpool.tx.length > 0) {
        for (let txhash of txpool.tx) {

            let tx = await API.getTxWithTxHash(txhash);

            if (tx && tx.data) {
                txns.push(tx);
            }
        }
    }
    return txns;
}